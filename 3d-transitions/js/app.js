const polygon = document.querySelector('.polygon');

const numberOfPages = document.querySelectorAll('.face').length;
const pageWidth = polygon.clientWidth;
const angle = 360 / numberOfPages;

const tz = Math.round((pageWidth / 2) / Math.tan(Math.PI / numberOfPages)) + 100;
let currentSelected = 0;

const init = () => {
    polygon.style.transform = `translateZ(-${tz}px) rotateY(${currentSelected * 60}deg)`;
    document.querySelectorAll('.face').forEach((el, index) => {
        el.style.transform = `rotateY(${index * 60}deg) translateZ(${tz}px)`;
    })
}

document.addEventListener('DOMContentLoaded', init);
window.addEventListener('resize', init);

document.querySelectorAll('.prev-button').forEach(prevButton => {
    prevButton.addEventListener('click', () => {
        animate(1);
    });
});

document.querySelectorAll('.next-button').forEach(nextButton => {
    nextButton.addEventListener('click', () => {
        animate(-1);
    });
})


function animate(increment) {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;

    document.body.style.overflowY = 'hidden';
    document.querySelectorAll('.face').forEach(el => el.style.display = 'flex');

    polygon.style.transform = `translateZ(-${tz * 1.5}px) rotateY(${currentSelected * 60}deg)`;
    polygon.addEventListener('transitionend', () => {
        currentSelected = currentSelected + increment;
        changeActive(increment < 0);
        polygon.style.transform = `translateZ(-${tz * 1.5}px) rotateY(${currentSelected * 60}deg)`;

        polygon.addEventListener('transitionend', () => {
            polygon.style.transform = `translateZ(-${tz}px) rotateY(${currentSelected * 60}deg)`;

            polygon.addEventListener('transitionend', () => {
                document.querySelectorAll(`.face:not(.active)`).forEach(el => el.style.display = 'none');
                document.body.style.overflowY = 'auto';
            }, { once: true })
        }, { once: true });
    }, { once: true })
}

function changeActive(next) {
    const currentActive = document.querySelector('.face.active');
    let nextActive = null;
    if (next) {
        nextActive = currentActive.nextElementSibling ?
            currentActive.nextElementSibling :
            document.querySelector('.face:first-child');
    } else {
        nextActive = currentActive.previousElementSibling ?
            currentActive.previousElementSibling :
            document.querySelector('.face:last-child');
    }

    currentActive.classList.remove('active');
    nextActive.classList.add('active');
}
